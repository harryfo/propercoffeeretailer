//creates firebase reference
var fireBase = new Firebase("https://propercoffee.firebaseio.com/");
fireBase.onAuth(authDataCallback);

//detect current log in status
function authDataCallback(authData) {
  if (authData) {
    //if user is logged in show form, hide login page and log it
    console.log("User " + authData.uid + " is logged in with " + authData.provider);
    document.getElementById("loginpage").style.display = "none";
    document.getElementById("orderpage").style.display = "block";
    document.getElementById("menubar").style.display = "block";
    } else {
    //if user is logged out log it, hide form and tell user to log in
    console.log("User logged out");
    }
};

//switch page to orders page and close other pages.
function openOrdersPage() {
    document.getElementById("orderpage").style.display = "block";
    document.getElementById("infopage").style.display = "none";
};

//switch page to info page and close other pages.
function openInfoPage() {
    document.getElementById("orderpage").style.display = "none";
    document.getElementById("infopage").style.display = "block";
};

//switch page to info page and close other pages.
function openRegisterPage() {
    document.getElementById("loginpage").style.display = "none";
    document.getElementById("registerpage").style.display = "block";
};

function registerFormSubmitted(form){
    var email = form.email.value;
    var password = form.password.value;
    
    fireBase.createUser({
        email : email,
        password : password
    }, function(error, userData) {
        if (error) {
            console.log("Error creating user:", error);
            document.getElementById("registerstatus").innerHTML = "An error occurred, please try again";
        } else {
            console.log("Successfully created user account with uid:", userData.uid);
            document.getElementById("loginpage").style.display = "block";
            document.getElementById("registerpage").style.display = "none";
        }
    })
};

function loginFormSubmitted(form){
    var email = form.email.value;
    var password = form.password.value;
    
    fireBase.authWithPassword({
        email : email,
        password : password
    }, function(error, authData) {
        if (error) {
            console.log("Login Failed!", error);
            document.getElementById("loginstatus").innerHTML = "Login Failed: Username or Password Incorrect";
        } else {
            console.log("Authenticated successfully with payload:", authData);
            document.getElementById("loginpage").style.display = "none";
            document.getElementById("orderpage").style.display = "block";
            document.getElementById("menubar").style.display = "block";
        }
    })
};